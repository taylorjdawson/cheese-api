import { normalize } from './utils'
import { Cheese } from './types'
import { createHash } from 'crypto'

export const getCheeseProperties = ($: CheerioAPI, elements: CheerioElement[]): Cheese =>
  Object.assign({}, ...elements.map((el: CheerioElement) => {
    let [property, value] = $(el).text().split(":").map(normalize)
    property = property ? property.split(' ')[0] : property
    if(property === 'made') {
      value = normalize($(el).text().split('Made from')[1])
      property = 'milk'
    }
    return {[property]: value}
}))

export const getCheeseId = (cheese: Cheese) =>
  ({
    id: createHash('sha1')
      .update(JSON.stringify(cheese))
      .digest('base64')
  })
