export const NOT_UNIQUE = "instance not unique"
export const ALPHABET: Array<string> = [
  's', 't', 'u', 'v', 'w', 'x',
  'y', 'z'
]
