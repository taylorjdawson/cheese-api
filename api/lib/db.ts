import { Client, query as q} from 'faunadb'
import {Cheese} from './types';
require('dotenv').config()

const client = new Client({ secret: process.env.FAUNA_DB_KEY || ''})

export const ingestCheese = async (cheese: Cheese) => {
  return client.query(q.Create(q.Collection('cheese'),
    { data: cheese}
  ))
}
