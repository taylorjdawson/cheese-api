const cheerio = require('cheerio')

export const normalize = (str: string): string => str.toLocaleLowerCase().trim().replace(/[\t\n]/g, '')

export const logError = (error: Error, method = '', data = {}) => console.error(error, method, data)

export const hasNextPage = (html: string, currentPage: number, $ = cheerio.load(html)) =>
  $(`#id_page_${--currentPage}`).prop('checked') && $(`#id_page_${++currentPage}`).length > 0
