
type Type = 'fresh-soft' | 'fresh-firm' | 'soft' | 'semi-soft' | 'semi-hard' | 'hard' | 'semi-firm' | 'firm'
type Milk =  'buffalo' | 'camel' | 'cow' | 'donkey' | 'goat' | 'mare' | 'moose' | 'reindeer' | 'sheep' | 'water buffalo' | 'yak'
type Country = "Afghanistan" | "Albania" | "Andorra" | "Argentina" | "Armenia" | "Australia" | "Austria" | "Azerbaijan" | "Bangladesh" | "Belarus" | "Belgium" | "Brazil" | "Bulgaria" | "Canada" | "Chile" | "China" | "countries throughout the world" | "Croatia" | "Cyprus" | "Czech Republic" | "Denmark" | "Eastern Mediterranean" | "Egypt" | "England" | "Finland" | "France" | "Georgia" | "Germany" | "Great Britain" | "Greece" | "Holland" | "Hungary" | "Iceland" | "India" | "Iran" | "Iraq" | "Ireland" | "Isle of Man" | "Israel" | "Italy" | "Japan" | "Jordan" | "Lebanon" | "Lithuania" | "Macedonia" | "Mauritania" | "Mexico" | "Mexico and Caribbean" | "Middle East" | "Mongolia" | "Nepal" | "Netherlands" | "New Zealand" | "Norway" | "Pakistan" | "Palestine" | "Poland" | "Portugal" | "Romania" | "Russia" | "Scotland" | "Serbia" | "Slovakia" | "Spain" | "Swaziland" | "Sweden" | "Switzerland" | "Syria" | "Tibet" | "Turkey" | "Ukraine" | "United Kingdom" | "United States" | "Wales"
export type CheeseProperty =
  'milk' | 'country' | 'region' | 'family' | 'type' | 'fat' | 'texture' | 'rind' | 'colour' | 'flavour' | 'vegetarian' | 'producers' | 'aroma' | 'synonyms'
type Texture = "brittle" | "buttery" | "chalky" | "chewy" | "close" | "compact" | "creamy" | "crumbly" | "crystalline" | "dense" | "dry" | "elastic" | "firm" | "flaky" | "fluffy" | "grainy" | "oily" | "open" | "runny" | "semi firm" | "smooth" | "soft" | "soft-ripened" | "spreadable" | "springy" | "sticky" | "stringy" | "supple"
type Colour = "blue" | "blue-grey" | "brown" | "brownish yellow" | "cream" | "golden orange" | "golden yellow" | "green" | "ivory" | "orange" | "pale white" | "pale yellow" | "pink and white" | "red" | "straw" | "white" | "yellow"

export interface Cheese {
  id: string
  imageUrl: string
  producers?: string
  flavour?: string
  aroma?: string
  name: string
  milk?: string
  family?: string
  fat?: string
  type?: string
  country?: string
  texture?: string
  colour?: string
  rind?: string
  synonyms?: string
  vegetarian?: string
  region?: string
}

export type IconName = 'fa-flask' | 'fa-flag' | 'fa-globe' | 'fa-child' | 'fa-folder-o' | 'fa-sliders' | 'fa-pie-chart' | 'fa-paint-brush' | 'fa-tint' | 'fa-spoon' | 'fa-leaf' | 'fa-industry' | 'fa-cutlery' | 'fa-language'
