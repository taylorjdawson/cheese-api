import {NowRequest, NowResponse} from '@vercel/node'
import got from 'got'

import { Cheese , CheeseProperty} from '../lib/types'
import {hasNextPage, logError, normalize} from '../lib/utils';
import {getCheeseId, getCheeseProperties} from '../lib/cheese';
import {ingestCheese} from '../lib/db';
import {ALPHABET, NOT_UNIQUE} from '../lib/constants';

const cheerio = require('cheerio')

const fs = require('fs')
const util = require('util')
const readFile = util.promisify(fs.readFile)

const BASE_URL: string = "https://www.cheese.com"

const loadPage = async () => {
  return readFile(__dirname + '/cheese_b.html')
}

const getLinks = async () =>
  Promise.all(ALPHABET.map( (letter: string) =>
    getCheeseHtml(letter).then( cheeseHtml => {
      const $ = cheerio.load(cheeseHtml)
      const links: string[] = []
      $('.cheese-item').each(function() {
        links.push($(this).find('a').attr('href'))
      })
      return links
    })
  )).then(values => values.flat())

const getCheeseHtml = async ( letter: string, currentPage: number = 1 ): Promise<string> =>
  got.get(`${BASE_URL}/alphabetical`, {
      searchParams: {
        per_page: 100,
        i: letter,
        page: currentPage
      }
    }
  ).then( async ({ body }) => hasNextPage(body, currentPage) ? body + await getCheeseHtml(letter, ++currentPage) : body)

const getCheese = ( link: string ) => got.get(`${BASE_URL}${link}`).then( async ({ body }): Promise<Cheese> => {
  const $ = cheerio.load(body)
  const name = normalize($('.catalog .detail .unit h1').text())
  const imageUrl = `${BASE_URL}${$('.cheese-image img').attr('src')}`
  const properties = getCheeseProperties($, $('.summary-points li').toArray())
  return {...properties, name, imageUrl}
}).catch(error => {
  logError(error, 'getCheese', { link })
})

const updateDB = async () => {
  const links = await getLinks()
  console.log('Links acquired: ' + links.length)
  for (const link of links) {
    const cheese = await getCheese(link)
    if(cheese) {
      console.log(`Parsed Cheese: ${cheese.name}`)
      await ingestCheese({ ...getCheeseId(cheese), ...cheese }).catch( error => {
        error.message !== NOT_UNIQUE ?
          logError(error.name, 'updateDB', { cheeseName: cheese.name}) :
          console.log(`Cheese "${cheese.name}" already exists`)
      })
      console.log(`Ingested Cheese: ${cheese.name}`)
    }
  }
}


(
  async () => {
    await updateDB()
  }
)()

// export default (request: NowRequest, response: NowResponse) => {
//   const {name = 'World'} = request.query
//   response.status(200).send(`Hello ${name}!`)
// }
